<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/app.css" type="text/css">
  <title>Instagram Scraper</title>

</head>
<body>
<div class="container home" style="text-align:center">
  <h1>Profile Detail</h1>
  @if(isset($edges))
    <div class="row">
      <div class="col-md-8">
        <h3><b>Username:</b> {{ $userName }}</h3>
        <h3><b>Number of Follwers:</b> {{ $noOfFollowers }}</h3>
      </div>
      <div class="col-md-4">
        <img src="{{ $profileImage }}" style="height: 100px; width:auto;">
      </div>
    </div>
    {{--{{ dd($edges) }}--}}
    <br>
    <hr>
    @foreach($edges as $edge)
      <h3>Image-{{$loop->iteration}}</h3>
      <hr>
      @if($edge->node->edge_media_to_caption->edges)
        <b>Caption</b>-{{ $edge->node->edge_media_to_caption->edges[0]->node->text }}
      @endif
      <img src="{{ $edge->node->thumbnail_src }}">
      <br>
      <hr>
    @endforeach
    @else
    <h1>User does not exist.</h1>
  @endif
</div>
</body>
</html>
